package ec.uio.picoyplaca.controlador;

import java.io.Serializable;
import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.Locale;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ViewScoped;
import javax.inject.Inject;
import lombok.Getter;
import lombok.Setter;
import org.apache.log4j.Logger;
import org.joda.time.DateTime;
import org.joda.time.format.DateTimeFormat;
import ec.uio.picoyplaca.administracion.IAdministracionServicio;
import ec.uio.picoyplaca.modelo.AutoDia;
import ec.uio.picoyplaca.util.MessageUtils;

/**
 * @author Christian
 *
 */

@ManagedBean
@ViewScoped
public class AutoDiaControlador implements Serializable {
	private static final long serialVersionUID = 1L;
	private final static Logger log = Logger.getLogger(AutoDia.class);
	@Getter
	@Setter
	private AutoDia autoDia;
	@Getter
	@Setter
	private String mensaje;

	@Inject
	private IAdministracionServicio administracionServicio;

	@PostConstruct
	public void inicializacion() {
		autoDia = new AutoDia();
	}

	/**
	 *METODO QUE OBTIENE LA FECHA INGRESADA POR EL USUARIO
	 *
	 */
	public void comprobarPP() {
		DateFormat formFech;
		Date date;
		try {
			formFech = new SimpleDateFormat("dd/MM/yyyy");
			date = formFech.parse(autoDia.getFecha());

			String nomDia = new SimpleDateFormat("EEEE", Locale.ENGLISH)
					.format(date);

			comprobarDia(nomDia);

		} catch (ParseException e) {
			e.printStackTrace();
		}
	}

	/**
	 * METODO QUE VERIFICA EL ULTIMO NUMERO DE LA PLACA SEGUN EL NOMBRE DEL DIA 
	 * @param nomDia
	 *            NOMBRE DEL DIA
	 * @throws ParseException
	 *             MENSAJE DE EXCEPCION
	 */
	public void comprobarDia(String nomDia) {
		try {
			switch (nomDia) {
			case "Monday":
				if (autoDia.getPlaca().substring(7).equals("1")
						|| autoDia.getPlaca().substring(7).equals("2")) {
					comprobarHora(autoDia.getHora());
				} else {
					MessageUtils.messageInfo("INFO",
							"USTED NO TIENE PICO Y PLACA", null);
				}
				break;

			case "Tuesday":
				if (autoDia.getPlaca().substring(7).equals("3")
						|| autoDia.getPlaca().substring(7).equals("4")) {
					comprobarHora(autoDia.getHora());
				} else {
					MessageUtils.messageInfo("INFO",
							"USTED NO TIENE PICO Y PLACA", null);
				}
				break;

			case "Wednesday":
				if (autoDia.getPlaca().substring(7).equals("5")
						|| autoDia.getPlaca().substring(7).equals("6")) {
					comprobarHora(autoDia.getHora());
				} else {
					MessageUtils.messageInfo("INFO",
							"USTED NO TIENE PICO Y PLACA", null);
				}
				break;

			case "Thursday":
				if (autoDia.getPlaca().substring(7).equals("7")
						|| autoDia.getPlaca().substring(7).equals("8")) {
					comprobarHora(autoDia.getHora());
				} else {
					MessageUtils.messageInfo("INFO",
							"USTED NO TIENE PICO Y PLACA", null);
				}
				break;

			case "Friday":
				if (autoDia.getPlaca().substring(7).equals("9")
						|| autoDia.getPlaca().substring(7).equals("0")) {
					comprobarHora(autoDia.getHora());
				} else {
					MessageUtils.messageInfo("INFO",
							"USTED NO TIENE PICO Y PLACA", null);
				}
				break;

			case "Saturday":
				MessageUtils.messageInfo("INFO",
						"ES SABADO, NO RIGE PICO Y PLACA", null);
				break;

			case "Sunday":
				MessageUtils.messageInfo("INFO",
						"ES DOMINGO, NO RIGE PICO Y PLACA", null);
				break;

			default:
				break;
			}
		} catch (Exception e) {
			MessageUtils.messageFatal("ERROR", "NO SE PUEDE COMPROBAR DIA",
					e.toString());
		}
	}

	/**
	 * METODO QUE COMPARA LAS HORAS DEPENDIENDO DE LOS INTERVALOS DE PICO Y
	 * PLACA
	 * @param hora
	 *            CAMPO DE HORA INGRESADA POR EL USUARIO
	 * @throws ParseException
	 *             MENSAJE DE EXCEPCION
	 */
	public void comprobarHora(String hora) {

		try {
			org.joda.time.format.DateTimeFormatter df = DateTimeFormat
					.forPattern("HH:mm");
			String hoInMa = "07:00";
			String hoFiMa = "10:00";
			String hoInTa = "16:00";
			String hoFiTa = "19:30";

			DateTime horaActual = df.parseLocalTime(hora).toDateTimeToday();

			DateTime limite1 = df.parseLocalTime(hoInMa).toDateTimeToday();
			DateTime limite2 = df.parseLocalTime(hoFiMa).toDateTimeToday();
			DateTime limite3 = df.parseLocalTime(hoInTa).toDateTimeToday();
			DateTime limite4 = df.parseLocalTime(hoFiTa).toDateTimeToday();

			if (horaActual.equals(limite1) || horaActual.equals(limite2)) {
				MessageUtils.messageInfo("INFO",
						"ES HORARIO DE PICO Y PLACA, NO PUEDE CONDUCIR", null);
			} else if (horaActual.isAfter(limite1)
					&& horaActual.isBefore(limite2)) {
				MessageUtils.messageInfo("INFO",
						"ES HORARIO DE PICO Y PLACA, NO PUEDE CONDUCIR", null);
			} else if (horaActual.equals(limite3) || horaActual.equals(limite4)) {
				MessageUtils.messageInfo("INFO",
						"ES HORARIO DE PICO Y PLACA, NO PUEDE CONDUCIR", null);
			} else if (horaActual.isAfter(limite3)
					&& horaActual.isBefore(limite4)) {
				MessageUtils.messageInfo("INFO",
						"ES HORARIO DE PICO Y PLACA, NO PUEDE CONDUCIR", null);
			} else {
				MessageUtils.messageInfo("INFO",
						"PUEDE CONDUCIR SIN PROBLEMAS", null);
			}
		} catch (Exception e) {
			MessageUtils.messageFatal("ERROR", "NO SE PUEDE COMPROBAR HORA",
					e.toString());
		}
	}
}
