package ec.uio.picoyplaca.administracion;

import ec.uio.picoyplaca.modelo.AutoDia;

public interface IAdministracionServicio {

	public AutoDia ppByPlaca (String placa) throws Exception;
}
